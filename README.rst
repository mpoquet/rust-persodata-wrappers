rust-persodata-wrappers
=======================

Wrapper tools to quickly update personal data files in CSV_.

Rationale
---------

- Mostly meant to test Rust out, nothing to see here.


.. _CSV: https://en.wikipedia.org/wiki/Comma-separated_values
.. _Rust: https://en.wikipedia.org/wiki/Rust_(programming_language)
