{ pkgs ? import (
    fetchTarball "https://github.com/NixOS/nixpkgs/archive/21.05.tar.gz") {}
}:

pkgs.rustPlatform.buildRustPackage rec {
  pname = "persodata-wrappers";
  version = "0.1-0";

  src = pkgs.lib.sourceByRegex ./. [
    "^Cargo\.lock"
    "^Cargo\.toml"
    "^src"
    "^src/.*\.rs"
    "^src/bin"
    "^src/bin/.*\.rs"
    "^src/bin/add-mass-entry"
    "^src/bin/add-mass-entry/.*\.rs"
  ];

  cargoSha256 = "sha256:1q0xb6mlrz6rshz5p90hnpw6vipbcdg9qbdpbfx54kbpwmq5s706";
}
