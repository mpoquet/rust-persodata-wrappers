use structopt::StructOpt;

use std::path::PathBuf;

use anyhow::Result;

use chrono::prelude::*;

use common::check_file;
use common::add_entry;

mod percentage;

const MASS_HEADER: &str = "date,mass,fat_percentage\n";

#[derive(StructOpt)]
/// Add a mass entry
struct Cli {
    /// New entry's mass (in kg)
    mass_kg: f64,

    /// New entry's fat percentage
    fat_percentage: Option<percentage::Percentage>,

    /// Mass file
    #[structopt(short, long, parse(from_os_str), default_value = "mass.csv")]
    filename: PathBuf,
}

fn main() -> Result<()> {
    let args = Cli::from_args();

    let fat_percentage = match args.fat_percentage {
        Some(x) => format!("{:.1}", x.value()),
        None => String::from("NA"),
    };

    let date = Local::now().format("%Y-%m-%d %H:%M:%S");
    let entry = format!("{},{},{}\n", date, args.mass_kg, fat_percentage);

    check_file(&args.filename, MASS_HEADER)?;
    add_entry(&args.filename, &entry)?;

    Ok(())
}
