use anyhow::Result;
use thiserror::Error;

#[derive(Clone)]
pub struct Percentage {
    value: f64,
}

#[derive(Error, Debug)]
pub enum PercentageError {
    #[error("Bad value: `{0}` not in [0,100]")]
    BadValue(f64),
}

impl Percentage {
    pub fn new(value: f64) -> Self {
        if value < 0.0 || value > 100.0 {
            panic!("Percentage value must be between 0 and 100, got {}.", value);
        }
        Percentage { value }
    }

    pub fn value(&self) -> f64 {
        self.value
    }
}

impl std::str::FromStr for Percentage {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self> {
        match s.parse::<f64>() {
            Ok(v) if (v >= 0.0 && v <= 100.0) => Ok(Percentage::new(v)),
            Ok(v) => Err(anyhow::Error::new(PercentageError::BadValue(v))),
            Err(e) => Err(anyhow::Error::new(e)),
        }
    }
}
