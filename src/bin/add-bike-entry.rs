use structopt::StructOpt;

use std::path::PathBuf;

use anyhow::Result;

use chrono::prelude::*;

use common::check_file;
use common::add_entry;

const SPORT_HEADER: &str = "date,minutes,sport\n";

#[derive(StructOpt)]
/// Add a bike (sport) entry
struct Cli {
    #[structopt()]
    /// The number of minutes done
    minutes: f64,

    /// Sport file
    #[structopt(short, long, parse(from_os_str), default_value = "sport.csv")]
    filename: PathBuf,
}

fn main() -> Result<()> {
    let args = Cli::from_args();

    let date = Local::now().format("%Y-%m-%d %H:%M:%S");
    let entry = format!("{},{},{}\n", date, args.minutes, "vélo");

    check_file(&args.filename, SPORT_HEADER)?;
    add_entry(&args.filename, &entry)?;

    Ok(())
}
