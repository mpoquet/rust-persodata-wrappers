use std::fs::File;
use std::io::ErrorKind;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::PathBuf;

use anyhow::Result;
use anyhow::bail;
use anyhow::ensure;

pub fn check_file(filename: &PathBuf, header: &str) -> Result<()> {
    let file = match File::open(filename) {
        Ok(f) => f,
        Err(err) => match err.kind() {
            ErrorKind::NotFound => match File::create(filename) {
                Ok(mut new_file) => {
                    new_file.write_all(header.as_bytes())?;
                    return Ok(())
                },
                Err(err) => bail!("Could not create file {:#?}: {}", filename, err),
            }
            _ => bail!("Cannot open file {:#?}: {}", filename, err),
        },
    };

    let mut reader = BufReader::new(file);
    let mut line = String::new();

    reader.read_line(&mut line)?;
    ensure!(line == header, format!("Unexpected file {:#?} content: Expected header line {:#?}, got {:#?}", filename, header, line));

    let header_cols = line.split(",").count();
    let mut i = 0;
    loop {
        i += 1;
        line.clear();
        match reader.read_line(&mut line) {
            Ok(0) => return Ok(()), // EOF
            Ok(_) => (),
            Err(err) => bail!("Cannot read line {} in {:#?}: {}", i, filename, err),
        };

        let cols = line.split(",").count();
        ensure!(cols == header_cols, format!("Unexpected file {:#?} content: Line {} {:#?} has {} columns but {} are assumed from header ({:#?})", filename, i+1, line, cols, header_cols, header));
        ensure!(line.ends_with("\n"), format!("Unexpected file {:#?} content: Line {} {:#?} has no final {:#?}", filename, i+1, line, "\n"));
    }
}

pub fn add_entry(filename: &PathBuf, entry: &str) -> Result<()> {
    let mut file = match std::fs::OpenOptions::new().append(true).open(filename) {
        Ok(f) => f,
        Err(err) => bail!("Cannot open file {:#?} (in append mode): {}", filename, err),
    };

    if let Err(err) = write!(file, "{}", entry) {
        bail!("Cannot write to file {:#?}: {}", filename, err);
    }

    Ok(())
}
